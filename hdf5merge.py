#title           :hdf5merge.py
#description     :merge hdf5 files
#author          :ivo
#date            :01.03.2016
#version         :1.0
#usage           :python hdf5merge.py /input/folder/ /output/file.hdf5
#notes           :
#python_version  :2.7
#==============================================================================
import h5py
import sys
import os
import numpy as np

dsets=[]

def find_datasets(name, obj):
    global dsets
    if isinstance(obj, h5py.Dataset):
        dsets.append({})
        dsets[-1]['key']=name
        dsets[-1]['shape']=obj.shape
        dsets[-1]['type']=obj.dtype

# xml directory
dirpath = sys.argv[1]
# Output file with ending
outfile = sys.argv[2]

#get names of hdf5 files
fname_list = [fname for fname in os.listdir(dirpath) if fname.endswith(".hdf5")]

#create output file
file_new = h5py.File(outfile, 'w')

#get keys,shape and type of all datasets
data=[]
for i in xrange(len(fname_list)):
    dsets=[]
    data.append({})
    data[-1]['handle']=h5py.File(dirpath+fname_list[i], 'r')
    data[-1]['handle'].visititems(find_datasets)
    data[-1]['dsets']=dsets


#create dataset in output file
print 'create output file structure'
for i in xrange(len(data[0]['dsets'])):
    shapei=0
    for j in xrange(len(data)):
        shapei+=data[j]['dsets'][i]['shape'][0]

    shape=list(data[0]['dsets'][i]['shape'])
    shape[0]=shapei
    types_=data[j]['dsets'][i]['type']
    dset_new = file_new.create_dataset(data[0]['dsets'][i]['key'], tuple(shape), dtype=types_)
    file_new.flush()

#fill all datasets with data from files
print 'copy data to output file'
index=np.zeros(len(data[-1]['dsets']),dtype=int)
for i in xrange(len(data[-1]['dsets'])):
    for j in xrange(len(data)):
        key=data[j]['dsets'][i]['key']
        var=data[j]['handle'][key]
        len_=var.shape[0]
        file_new[key][index[i]:index[i]+len_,]=var[:]
        index[i]+=len_
        file_new.flush()

#close all files
print 'close all files'
for j in xrange(len(data)):
    data[j]['handle'].close()

file_new.close()
